<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view("halaman.form");
    }

    public function kirim(request $request){
        $nama = $request['nama'];
        $nim = $request['nim'];
        $fakultas = $request['fakultas'];
        return view("halaman.mantab", compact("nama", "nim", "fakultas"));
    }
}
