@extends('layout.master')

@section('judul')
    Edit Data :
@endsection

@section('content')
<a href="/cast" class="btn btn-danger btn-bg mb-3" >Back</a>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="5">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-info">Submit</button>
  </form>

@endsection