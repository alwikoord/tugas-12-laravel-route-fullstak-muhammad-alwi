@extends('layout.master')

@section('judul')
    Data Detail :
@endsection

@section('content')
<a href="/cast" class="btn btn-danger btn-bg mb-3" >Back</a>
<h3>Nama Lengkap : {{$cast->nama}}</h3>
<h3>Umur : {{$cast->umur}}</h3>
<strong>{{$cast->bio}}</strong>

@endsection