@extends('layout.master')

@section('judul')
Selamat Datang di Digital Library
@endsection

@section('content')
<p>adalah sebuah perpustakaan digital yang dijadikan sistem digital. <br>
    Perpustakaan ini memiliki berbagai layanan elektonik, seperti pembuatan kartu dan peminjaman secara online</p>
    
    <h3>Berikut adalah layanan Perpustakaan Digital : </h3>
    <ol>
        <li>Pengisian Form pembuatan kartu <a href="/form">disini</a></li>
        <li>Peminjaman Buku Online (Silahkan Hubungi Staff)</li>
        <li>Pengambilan surat bebas pustaka untuk mahasiswa akhir (Silahkan Hubungi Staff)</li>
    </ol>
@endsection
    
