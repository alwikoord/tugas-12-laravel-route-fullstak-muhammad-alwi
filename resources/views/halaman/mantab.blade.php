<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ucapan Terimakasih</title>
</head>
<body>
    <h1>Selamat Datang</h1>
    <p>Nama : {{$nama}} </p>
    <p>Nim  : {{$nim}} </p>
    <p>Fakultas  : {{$fakultas}} </p> <br>

    <strong>Kami ucapkan terimakasih banyak sudah menggunakan layanan perpustakaan online kami...<br>
    Info selanjutnya akan kami umumkan melalui email, stay tune yaa.. !!! </strong>

    <h3>Silahkan daftar lagi <a href="/form">disini</a> </h3>
</body>
</html>