@extends('layout.master')

@section('judul')
    Silahkan isi form berikut :
@endsection

@section('content')
<form action="/mantab" method="post"> 
    @csrf
    <label>Masukkan Nama :</label> <br>
    <input type="text" name="nama"> <br><br>

    <label >Nim :</label> <br>
    <input type="number" name="nim"> <br><br>

    <label>Program pendidikan :</label> <br>
    <input type="radio" name="program pendidikan" value="Program Strata 1 (S1)"> <label>Pogram Strata 1 (S1)</label><br>
    <input type="radio" name="program pendidikan" value="Program Magister (S2"> <label>Program Magister (S2)</label><br>
    <input type="radio" name="program pendidikan" value="Program Doktoral (S3)"> <label>Prograam Doktoral (S3)</label><br><br>

    <label for="Masukkan Email dengan teliti">Email :</label><br>
    <input type="email" id="email" name="email"> <br><br>

    <label>Alamat :</label> <br>
    <textarea name="alamat" rows="8" cols="25"></textarea> <br><br>

    <label>Asal Fakultas :</label> <br>
    <select name="fakultas">
        <option selected>Silahkan Pilih</option>
        <option>Fakultas Tarbiyyah</option>
        <option>Fakultas Ushuluddin</option>
        <option>Fakultas Humaniora</option>
        <option>Fakultas Sains dan Teknologi</option>
        <option>Fakultas Kesehatan</option>
        <option>Fakultas Syariah</option>
        <option>Fakultas Ekonomi dan Manajemen</option>
    </select> <br><br>
    <input type="submit" value="kirim">
    <input type="Reset">
</form>

@endsection